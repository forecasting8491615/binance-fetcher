ipython==8.23.0
jupyter==1.0.0
pandas==2.2.1
psycopg2-binary==2.9.9
requests==2.31.0
pyyaml==6.0.1