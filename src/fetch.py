import logging as logger
import os
import requests

import pandas as pd
import psycopg2
import psycopg2.extras as extras
import yaml

logger.basicConfig()
logger.getLogger().setLevel(logger.INFO)


def get_last_date(conn, symbol):
    cursor = conn.cursor()
    cursor.execute(f"select max(open_time) from trade_data where symbol='{symbol}';")
    data = cursor.fetchall()
    cursor.close()

    return data[0][0]


def get_symbols_data(mindsdb_conf, symbols):
    url = f"{mindsdb_conf['host']}:{mindsdb_conf['port']}/api/sql/query"

    # connect to binance
    resp = requests.post(url, json={'query': """CREATE DATABASE my_binance
                                                WITH ENGINE = 'binance';"""})

    logger.info(resp.json())

    # get symbols data

    data_symbols = {}
    for symbol in symbols:
        last_timestamp = get_last_date(conn, symbol)

        from_time = str(last_timestamp) if last_timestamp is not None else mindsdb_conf[
            'min_date']

        query = f"""SELECT open_time, symbol, open_price, high_price, low_price, 
                close_price, volume, number_of_trades
                FROM my_binance.aggregated_trade_data
                WHERE symbol = '{symbol}'
                AND open_time > '{from_time}'
                AND interval = '1h';"""

        resp = requests.post(url, json={'query': query})
        data = pd.DataFrame(resp.json()['data'],  columns=resp.json()['column_names'])
        data['open_time'] = pd.to_datetime(data['open_time'], unit='s')
        data_symbols[symbol] = data

    return data_symbols


def push_to_postgres(data_symbols, conn, table):
    for symbol in data_symbols:
        df = data_symbols[symbol]
        tuples = [tuple(x) for x in df.to_numpy()]

        cols = ','.join(list(df.columns))
        # SQL query to execute
        query = "INSERT INTO %s(%s) VALUES %%s" % (table, cols)
        cursor = conn.cursor()
        try:
            extras.execute_values(cursor, query, tuples)
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print("Error: %s" % error)
            conn.rollback()
            cursor.close()
            return 1
        logger.info("the dataframe is inserted")
        cursor.close()

    # remove duplicates: somehow it repeats the last timestamp regardless of using >
    # and not >= in the query
    query_remove_duplicates = """DELETE FROM trade_data T1
                                    USING   trade_data T2
                                    WHERE   T1.ctid < T2.ctid  
                                    AND T1.symbol  = T2.symbol
                                    AND T1.open_time  = T2.open_time;"""

    cursor = conn.cursor()
    cursor.execute(query_remove_duplicates)
    conn.commit()

    cursor.close()


if __name__ == "__main__":
    config = yaml.safe_load(open("config/config.yaml"))
    symbols = config['symbols']
    pg_creds = {
                'user': os.environ['PGUSER'],
                'host': os.environ['PGHOST'],
                'psw': os.environ['PGPASSWORD']
                }
    table = config['postgres']['table']

    conn = psycopg2.connect(f"user={pg_creds['user']} host="
                            f"{pg_creds['host']} password={pg_creds['psw']}")

    data_symbols = get_symbols_data(config['minds-db'], symbols)

    push_to_postgres(data_symbols, conn, table)